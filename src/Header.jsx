// src/Header.jsx
import React from 'react';
import './App.css';

import './Header.module.css'; // Ensure you import the CSS module if you use one

function Header() {
  return (
    <header>
      <div className="container">
        <a id="logo" href="/">Cartify</a>
        <nav>
          <ul>
            <li><a href="#home">Home</a></li>
            <li><a href="#about">About</a></li>
            <li><a href="#products">Products</a></li>
            <li><a href="#contact">Contact</a></li>
          </ul>
        </nav>
        <div className="actionLinks">
          <a href="#">
            <span className="material-symbols-outlined">favorite</span>
          </a>
          <a href="#">
            <span className="material-symbols-outlined">local_mall</span>
          </a>
        </div>
      </div>
    </header>
  );
}


export default Header;
