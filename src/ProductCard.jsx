import React from 'react';
import './ProductCard.css'; // Ensure you import the CSS module

function ProductCard(props) {
  function addToCart() {
    alert("Added to cart");
  }

  return (
    <article className="product">
      <img
        className={props.image ? 'product-image' : 'placeholder-image'}
        src={props.image ? props.image : 'http://via.placeholder.com/640x360'}
        alt={props.title}
      />
      <div className="productDetails">
        <h3 className="h6">{props.title}</h3>
        <div>
          <span>★</span>
          <span>★</span>
          <span>★</span>
          <span>★</span>
          <span>☆</span>
        </div>
        <div className="priceAndButton">
          <span className="p">{props.price}</span>
          <button onClick={addToCart} className="cartbutton">Add to cart</button>
        </div>
      </div>
    </article>
  );
}

export default ProductCard;
