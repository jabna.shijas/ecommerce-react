import React from 'react';
import Header from './Header';
import './App.css';
import ProductCard from './ProductCard';

function App() {
  const products = [
    {
      id: 1,
      image: 'https://cdn.shopify.com/s/files/1/0785/1674/8585/files/0932.jpg?v=1717134297&width=552&crop=center',
      title: 'Tier Dress With Smocking Detail',
      price: 2400,
    },
    {
      id: 2,
      image: 'https://cdn.shopify.com/s/files/1/0785/1674/8585/files/7thMayVirgio-6436.jpg?v=1717055105&width=552&crop=center',
      title: 'Tier Dress With Smocking Detail',
      price: 2040,
    },
    {
      id: 3,
      image: 'https://cdn.shopify.com/s/files/1/0785/1674/8585/files/7thMayVirgio-7427.jpg?v=1717051437&width=552&crop=center',
      title: 'Breathable Cotton Bubble Dress With Balloon Sleeves',
      price: 2300,
    },
    {
      id: 4,
      image: 'https://cdn.shopify.com/s/files/1/0785/1674/8585/files/21MayVirgio-00221.jpg?v=1717051471&width=552&crop=center',
      title: 'Breathable cotton fit and flare schiffili dress',
      price: 2490,
    },
    {
      id: 5,
      image: 'https://cdn.shopify.com/s/files/1/0785/1674/8585/files/24MayVirgio-00951.jpg?v=1717068491&width=552&crop=center',
      title: 'Cinched Dress with Pockets',
      price: 2200,
    },
    {
      id: 6,
      image: 'https://cdn.shopify.com/s/files/1/0785/1674/8585/files/1_VWWDR2336012802_74047dbe-442a-42f7-9a5e-d599bc67e86b.jpg?v=1706284394&width=552&crop=center',
      title: 'Breathable Denim Cotton Dress',
      price: 2200,
    },
    {
      id: 7,
      image: 'https://cdn.shopify.com/s/files/1/0785/1674/8585/files/6_VWWDR2336017103.jpg?v=1714051813&width=552&crop=center',
      title: 'Breathable cotton sweetheart neckline dress',
      price: 1500,
    },
    {
      id: 8,
      image: '',
      title: 'Breathable cotton fit and flare schiffili dress',
      price: 2490,
    }
  ];

  return (
    <>
      <Header />
      <main>
        <section id="section1">
          <div className="container">
            <div id="images">
              <img id="mainImage" src="https://m.media-amazon.com/images/I/71KwC73OHaL._SY741_.jpg" alt="Main" />
          
                <img id="image1" src="https://m.media-amazon.com/images/I/41swNwfYbeL._SX38_SY50_CR,0,0,38,50_.jpg" alt="Thumbnail 1" />
                <img id="image2" src="https://m.media-amazon.com/images/I/41TayUGBzIL._SX38_SY50_CR,0,0,38,50_.jpg" alt="Thumbnail 2" />
                <img id="image3" src="https://m.media-amazon.com/images/I/418K97+7jvL._SX38_SY50_CR,0,0,38,50_.jpg" alt="Thumbnail 3" />
                <img id="image4" src="https://m.media-amazon.com/images/I/41dxMBBNvtL._SX38_SY50_CR,0,0,38,50_.jpg" alt="Thumbnail 4" />
              </div>
            
            <div>
              <h1 id="productTitle">Short Smocking Printed Cami Dress</h1>
              <span className="h3 price">
                MRP ₹<span id="price">1,490</span>
              </span>
              <ul className="variants">
                <li className="variant">
                  <img src="https://m.media-amazon.com/images/I/41lrHtbQb1L._SX38_SY50_CR,0,0,38,50_.jpg" alt="Variant 1" />
                </li>
                <li className="variant">
                  <img src="https://m.media-amazon.com/images/I/41qbr76RxkL._SX38_SY50_CR,0,0,38,50_.jpg" alt="Variant 2" />
                </li>
                <li className="variant">
                  <img src="https://m.media-amazon.com/images/I/41ZnQ6osLmL._SX38_SY50_CR,0,0,38,50_.jpg" alt="Variant 3" />
                </li>
                <li className="variant">
                  <img src="https://m.media-amazon.com/images/I/41JBac4zUqL._SX38_SY50_CR,0,0,38,50_.jpg" alt="Variant 4" />
                </li>
              </ul>
              <p id="productDescription">
                Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ratione doloremque explicabo cum. Iste aspernatur alias quidem ullam, velit optio vero itaque. Aliquid deserunt eum incidunt nemo qui quo, rerum ad? Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem necessitatibus hic dicta suscipit consequatur velit perferendis eius pariatur fuga fugiat. Nostrum hic corporis magnam velit beatae iusto officiis asperiores totam?
              </p>
              <div id="productButtons">
                <button className="buttonPrimary">Add to cart</button>
                <button className="buttonSecondary">Buy Now</button>
              </div>
            </div>
          </div>
        </section>
        <section id="section2">
          <div className="container">
            <h2 className="h4">Related products</h2>
            <div className="productsList" id="listingDiv">
              {products.map((product) => (
                <ProductCard key={product.id} title={product.title} price={product.price} image={product.image} />
              ))}
            </div>
          </div>
        </section>
      </main>
    </>
  );
}

export default App;
